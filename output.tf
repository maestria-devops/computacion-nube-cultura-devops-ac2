output "web_endpoint" {
    value = "http://${azurerm_storage_account.static_storage.primary_web_host}"
}

output "cdn_endpoint" {
    value = "http://${azurerm_cdn_endpoint.endpoint.fqdn}"
}